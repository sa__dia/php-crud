<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="<?php echo base_url();?>css/design.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.min.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>student list</title>
</head>
<body>
    <div class="container">
        <h2>all student data</h2>
        <a href = "get" class="btn btn-primary list_add_button">add new student</a> 
        <!--<button href = "<?php echo base_url();?>Student_controller/insert" class="btn btn-primary list_add_button">add new student</button> -->
        <table class="data_list">
            <thead>
                <th>ID</th>
                <th>Name</th>
                <th>Age</th>
                <th>Action</th>
            </thead>
            <tbody>               
                    <?php foreach($record->result() as $row){
                    echo "<tr>";
                        echo "<td>".$row->id." </td>";
                        echo "<td>".$row->name." </td>";
                        echo "<td>".$row->age." </td>";
                       
                        echo "<td><a class = 'btn btn-danger' href='delete?id=".$row->id."'>Delete</a></td>";                       
                        echo "<td><a class = 'btn btn-info' href='get/".$row->id."'>Edit</a></td>";                       
                        
                    echo "</tr>";    
                        }?>                    
                </tr>           
            </tbody>
        </table>
    </div>
</body>
</html>