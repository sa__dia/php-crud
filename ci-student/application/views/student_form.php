<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<?php echo base_url();?>css/design.css">
    <link rel="stylesheet" href=" <?php echo base_url();?>css/bootstrap.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>student form</title>
</head>
<body>


<?php 
        $id   = '';
        $name   = '';
        $age   = '';
        if (!empty($row)) { 
        $id = $row->id;    
        $name = $row->name;
        $age = $row->age;
        }
        ?> 

    <div class="container">
        <h1>Student form</h1>
        <form class="data_list" action="<?php echo base_url();?>index.php/student_controller/save" method="post">
            <input type="number" name="id"  value="<?php echo $id; ?>"  hidden>
            <label>Name</label>
            <input type="text" name="name"   value="<?php echo $name; ?>">
            <br> <br>
            <label>Age</label>
            <input type="number" name="age"  value="<?php echo $age; ?>">
            <br> <br>            
           <button  type="submit" class="btn btn-primary">Save</button>    
        </form>
    </div>
</body>
</html>