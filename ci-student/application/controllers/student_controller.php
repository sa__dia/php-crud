<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_controller extends CI_Controller {
    
    function __construct()
     {
         parent :: __construct();        
         $this->load->helper('url');
        $this->load->model('Student_model');               
     }
    
   
    function all(){        
        $data['record'] = $this->Student_model->getAll();
        $this->load->view('student_list', $data);       
   }
   function delete(){
       $id  = $this->input->get("id");      
       $this->Student_model->deletedata($id);
       redirect('student_controller/all', 'refresh');
    }

   function get($id=0){   

    if($id>0){
        $data['row']=$this->Student_model->get($id); 
        $this->load->view('student_form',$data);      
    }else{
         $this->load->view('student_form');    
    } 
  }
    

   function save(){    
        $id = $this->input->post('id');       
        $name = $this->input->post('name');
        $age = $this->input->post('age');
        
        if($id>0){
            $this->Student_model->update($id ,$name, $age);
        }else{
            $this->Student_model->insert($name, $age);
        }
        redirect('/student_controller/all', 'refresh');     
   }
  
}
?>